module Additions
  class Decorator < Decorator
    type :addition

    property :first_variable
    property :second_variable
    property :notation
    property :result
  end
end
