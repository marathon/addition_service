require_relative 'decorator'
require_relative 'record'
module Additions
  class App < AdditionService::App

    post 'additions' do
      if params['additions']
        Additions::Decorator.represent(Additions::Record.find(params['additions'])).to_json
      else
        "Missing parameter: 'additions'"
      end
    end

  end
end
