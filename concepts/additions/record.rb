module Additions
  class Record < Resource
    attr_accessor :first_variable, :second_variable

    def self.find(params)
      self.new(first_variable: params["first_variable"], second_variable: params["second_variable"])
    end

    def initialize(first_variable: nil, second_variable: nil)
      @first_variable = first_variable.to_i
      @second_variable = second_variable.to_i
    end

    def result
      @first_variable + @second_variable
    end

    def notation
      "#{first_variable} + #{second_variable} = #{result}"
    end

  end
end
